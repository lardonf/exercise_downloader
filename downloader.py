#!/usr/bin/env python3
# Author: Fadri Lardon
import os
import urllib.request
import urllib.error
import json
import re
import sys
import argparse


parser = argparse.ArgumentParser(
            prog="downloader",
            description="file downloader intended for my ETH exercise series and scripts",
            epilog="requires a semesters.json in your active directory"
        )

parser.add_argument("week", nargs="?", default=15, type=int, help="the week you would like to download your exercise for, the default is 15")
parser.add_argument("-a", "--all", action="store_true", help="download up the the given week instead of just the given week")
parser.add_argument("-s", "--semester", type=int, help="the semester you would like to download stuff from")

args = parser.parse_args()
# print(args)

with open("semesters.json", "r") as f:
    data = json.load(f)

name = data["name"]
data = data["semesters"]

sem = max(map(int, data))

if args.semester is not None:
    sem = min(args.semester, sem)


def get_shorthand(lecture: str):
    return "".join((i[:4] for i in re.split(" |_", lecture)))

def ensure_path_existence(path: str):
    if os.path.exists(path):
        return
    os.mkdir(path)


def setup_template(lecture: str, week: int):

    shorthand = get_shorthand(lecture)
    filename = os.path.join(lecture, "Exercises", f"{shorthand}_{week}.tex")
    # I do not wish for my files to get overwritten
    if os.path.exists(filename):
        return

    with open(filename, "w") as file:
        print("\\documentclass{amsart}", file=file)
        print("\\usepackage{amsmath}", file=file)
        print("\\usepackage{amssymb}", file=file)
        print(f"\n\\author{{{name}}}", file=file)
        print("\\title{{{} Series {}}}".format(
            lecture.replace("_", " "), week), file=file)
        print("\n\\begin{document}\n\\maketitle", file=file)
        print("\\begin{enumerate}\n", file=file)
        print("\\end{enumerate}", file=file)
        print("\\end{document}", file=file)


# returns True on success and False on failure
def download_week(lec: dict, week: int):
    ensure_path_existence(lec["name"])
    
    num = week + (lec["offset"] if "offset" in lec else 0)
    
    if ("file" not in lec) or ("url" not in lec):
        return

    f = lec["file"].format(num)

    try:
        urllib.request.urlretrieve(f"{lec['url']}/{f}", os.path.join(lec['name'], f))
    except urllib.error.HTTPError as e:
        print(lec["name"].ljust(max((len(l["name"]) for l in lecs))), f"week {week:2} Download failed with code ", e.code)
        return False
    except urllib.error.URLError as e:
        print(lec["name"].ljust(max((len(l["name"]) for l in lecs))), f"week {week:2} URL resolution failed with: ", e.reason)
        return False

    ensure_path_existence(os.path.join(lec["name"], "Exercises"))
    setup_template(lec["name"], num)

    return True


ensure_path_existence(f"Semester_{sem}")

if os.path.lexists("current"):
    os.unlink("current")

os.symlink(f"Semester_{sem}", "current", target_is_directory=True);
os.chdir(f"Semester_{sem}")

lecs = data[str(sem)]

for lec in lecs:
    if args.all:
        for i in range(1, args.week + 1):
            if not download_week(lec, i):
                break;
    else:
        download_week(lec, args.week)

print("ALL DONE! :)")
